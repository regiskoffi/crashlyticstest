package com.android.crashlyticstest.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.android.crashlyticstest.apdater.InfiniteScrollListener;
import com.android.crashlyticstest.R;
import com.android.crashlyticstest.Utils;
import com.android.crashlyticstest.activity.BaseActivity;
import com.android.crashlyticstest.apdater.CommentListAdapter;
import com.android.crashlyticstest.model.Comment;
import com.android.crashlyticstest.model.Issue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CommentListDialogFragment extends DialogFragment{

    public static  final String TAG = "CommentListDialogFragment";

    private ListView listView;
    private CommentListAdapter commentListAdapter;
    BaseActivity baseActivity;
    private Issue issue;



    public static CommentListDialogFragment newInstance() {
        return new CommentListDialogFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.white);
        baseActivity = (BaseActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.issue_list_activity, container, false);
        listView = (ListView) view.findViewById(R.id.list_view);
        commentListAdapter = new CommentListAdapter(getActivity(),null);
        listView.setAdapter(commentListAdapter);
        if(issue.getComments()>30){
            listView.setOnScrollListener(new InfiniteScrollListener(30) {
                @Override
                public void loadMore(int page, int totalItemsCount) {
                    commentListAdapter.setPage(page);
                    StringBuilder urlBuilder = new StringBuilder(issue.getCommentsUrl());
                    urlBuilder.append("page=" + page);
                    new IssueListTask().execute(urlBuilder.toString());
                }
            });
        }

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        new IssueListTask().execute(issue.getCommentsUrl());
    }


    private class IssueListTask extends AsyncTask<String,Void,List<Comment>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(List<Comment> commentList) {
            super.onPostExecute(commentList);
            if(commentList==null&&commentListAdapter.getPage()==0){
                CommentListDialogFragment.this.dismiss();
                return;
            }
            commentListAdapter.newData(commentList);
        }

        @Override
        protected List<Comment> doInBackground(String... params) {
            String response = baseActivity.getService().getMethod(params[0]);
            List<Comment> commentList = null;
            if(response!=null) {
                commentList =  parseResponse(response);
            }
            return  commentList;
        }
    }

    private List<Comment> parseResponse(String response) {
        List<Comment> commentList = new ArrayList<>();
        if (!Utils.isEmpty(response)) {
            try {
                JSONArray rootArray = new JSONArray(response);
                for(int i=0;i<rootArray.length();i++){
                    JSONObject jsonObject =  rootArray.getJSONObject(i);
                    final Comment comment = parseComment(jsonObject);
                    commentList.add(comment);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return commentList;
    }

    private Comment parseComment(JSONObject jsonObject){
        String body = jsonObject.optString("body");
        String author = null;
        JSONObject userObj = jsonObject.optJSONObject("user");
        if(userObj!=null){
            author = userObj.optString("login");
        }
        Comment comment = new Comment(author,body);
        return comment;
    }

    public Issue getIssue() {
        return issue;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
    }
}
