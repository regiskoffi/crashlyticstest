package com.android.crashlyticstest;

import android.support.annotation.Nullable;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Utils {
    private static final String TAG = "Utils";

    public static Date formatDate(String dateStr){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            return sdf.parse(dateStr);
        }catch (ParseException e){
            Log.e(TAG,""+e.getMessage());
        }
        return null;
    }

    public static boolean isEmpty(@Nullable CharSequence str) {
        if(str == null){
            return true;
        }else if(str.length() == 0){
            return true;
        }
        return false;
    }
}
