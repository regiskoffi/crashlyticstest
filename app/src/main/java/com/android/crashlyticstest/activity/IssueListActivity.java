package com.android.crashlyticstest.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.crashlyticstest.apdater.InfiniteScrollListener;
import com.android.crashlyticstest.R;
import com.android.crashlyticstest.Utils;
import com.android.crashlyticstest.apdater.IssueListAdapter;
import com.android.crashlyticstest.fragment.CommentListDialogFragment;
import com.android.crashlyticstest.model.Issue;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import com.android.crashlyticstest.request.UrlConstants;

public class IssueListActivity extends BaseActivity {

    private final static String TAG = "IssueListActivity";

    private ListView listView;
    private TextView emptyView;
    private IssueListAdapter issueListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issue_list_activity);
        setTitle(getString(R.string.issue_list_title));
        listView = (ListView) findViewById(R.id.list_view);
        emptyView =(TextView) findViewById(R.id.empty_view);
        issueListAdapter = new IssueListAdapter(this,null);
        listView.setAdapter(issueListAdapter);

        new IssueListTask().execute(UrlConstants.ISSUES_LIST_URL);

        listView.setOnScrollListener(new InfiniteScrollListener(30) {
            @Override
            public void loadMore(int page, int totalItemsCount) {
                StringBuilder urlBuilder = new StringBuilder(UrlConstants.ISSUES_LIST_URL);
                urlBuilder.append("page=" + page);
                new IssueListTask().execute(urlBuilder.toString());
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Issue issue = (Issue) issueListAdapter.getItem(position);
                if(issue.getComments()>0) {
                    CommentListDialogFragment commentDialog = CommentListDialogFragment.newInstance();
                    commentDialog.setIssue(issue);
                    commentDialog.show(getSupportFragmentManager(), commentDialog.getTag());
                }
            }
        });
    }

    private class IssueListTask extends AsyncTask<String,Void,List<Issue>>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressDialog();
        }

        @Override
        protected void onPostExecute(List<Issue> issueList) {
            super.onPostExecute(issueList);
            hideProgressDialog();
            issueListAdapter.newData(issueList);
            if(issueListAdapter.getCount()==0){
                emptyView.setVisibility(View.VISIBLE);
            }else{

            }
        }

        @Override
        protected List<Issue> doInBackground(String... params) {
            String response = getService().getMethod(params[0]);
            List<Issue> issueList = null;
            if(response!=null) {
                issueList = parseRequestResponse(response);
            }
            return  issueList;
        }
    }

    private List<Issue> parseRequestResponse(String response){
        List<Issue> issueList =  new ArrayList<>();
        try {
            if(response.startsWith("[")) {
                JSONArray rootArray = new JSONArray(response);
                for (int i = 0; i < rootArray.length(); i++) {
                    JSONObject jsonObject = rootArray.getJSONObject(i);
                    Issue issue = parseIssue(jsonObject);
                    issueList.add(issue);

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return issueList;
    }

    private Issue parseIssue(JSONObject jsonObject){
        String title =  jsonObject.optString("title");
        String body = jsonObject.optString("body");
        if(!Utils.isEmpty(body)&&body.length()>140){
            body = new StringBuilder(body.substring(0,140)).append("...").toString();
        }
        String commentUrl =  jsonObject.optString("comments_url");
        Issue issue = new Issue();
        issue.setTitle(title);
        issue.setBody(body);
        issue.setCommentsUrl(commentUrl);
        issue.setComments(jsonObject.optInt("comments",0));
        return  issue;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_refresh) {
            issueListAdapter.clearData();
            new IssueListTask().execute(UrlConstants.ISSUES_LIST_URL);
        }

        return super.onOptionsItemSelected(item);
    }
}
