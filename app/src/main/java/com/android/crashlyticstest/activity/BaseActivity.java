package com.android.crashlyticstest.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.android.crashlyticstest.R;

import com.android.crashlyticstest.request.BaseService;

public class BaseActivity extends AppCompatActivity {

    private BaseService service;

    protected ActionBar actionBar;

    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        actionBar = getSupportActionBar();

    }

    protected void setTitle(String title){
        if(title!=null){
            actionBar.setTitle(title);
        }
    }

    public BaseService getService() {
        if (service == null) {
            service = new BaseService();
        }
        return service;
    }

    protected void showProgressDialog(){
        if(progressDialog==null){
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getString(R.string.loading));
        }
        progressDialog.show();
    }

    protected void hideProgressDialog(){
        if(progressDialog!=null){
            progressDialog.dismiss();
        }
    }

}
