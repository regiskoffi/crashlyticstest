package com.android.crashlyticstest.apdater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.crashlyticstest.R;
import com.android.crashlyticstest.Utils;
import com.android.crashlyticstest.model.Comment;

import java.util.List;

public class CommentListAdapter extends BaseAdapter {

    private static final String TAG ="CommentListAdapter";

    protected List<Comment> commentList;
    protected Context context;
    private int page = 0;

    public CommentListAdapter(Context context, List<Comment> objects) {
        this.commentList = objects;
        this.context = context;
    }

    public void newData(List<Comment> commentList) {
        if(this.commentList ==null){
            this.commentList = commentList;
        }else{
            this.commentList.addAll(commentList);
        }

        notifyDataSetChanged();
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public void clearData(){
        this.commentList = null;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return commentList == null ? 0 : commentList.size();
    }

    @Override
    public Object getItem(int position) {
        return commentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Comment commentModel = commentList.get(position);
        if (convertView == null) {
            LayoutInflater mInflater =
                    ((Activity) context).getLayoutInflater();
            convertView = mInflater.inflate(R.layout.comment_view_item, null);
            viewHolder = new ViewHolder();
            viewHolder.commentAuthorView = (TextView) convertView.findViewById(R.id.comment_author);
            viewHolder.commentBodyView = (TextView) convertView.findViewById(R.id.comment_body);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(!Utils.isEmpty(commentModel.getAuthor())) {
            viewHolder.commentAuthorView.setText(String.format(convertView.getResources().getString(R.string.author), commentModel.getAuthor()));
        }
        if(!Utils.isEmpty(commentModel.getBody())) {
            viewHolder.commentBodyView.setText(commentModel.getBody());
        }

        return convertView;
    }

    static class ViewHolder {
        private TextView commentAuthorView;
        private TextView commentBodyView;
    }


}
