package com.android.crashlyticstest.apdater;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.crashlyticstest.R;
import com.android.crashlyticstest.Utils;
import com.android.crashlyticstest.model.Issue;

import java.util.List;

public class IssueListAdapter extends BaseAdapter {

    private static final String TAG = "IssueListAdapter";

    protected List<Issue> issueList;
    protected Context context;

    public IssueListAdapter(Context context, List<Issue> objects) {
        this.issueList = objects;
        this.context = context;
    }

    public void newData(List<Issue> issueList) {
        if(this.issueList ==null){
            this.issueList = issueList;
        }else{
            this.issueList.addAll(issueList);
        }

        notifyDataSetChanged();
    }

    public void clearData(){
        this.issueList = null;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return issueList == null ? 0 : issueList.size();
    }

    @Override
    public Object getItem(int position) {
        return issueList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Issue issueModel = issueList.get(position);
        if (convertView == null) {
            LayoutInflater mInflater =
                    ((Activity) context).getLayoutInflater();
            convertView = mInflater.inflate(R.layout.issue_view_item, null);
            viewHolder = new ViewHolder();
            viewHolder.issueTitleView = (TextView) convertView.findViewById(R.id.issue_title);
            viewHolder.issueBodyView = (TextView) convertView.findViewById(R.id.issue_body);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (!Utils.isEmpty(issueModel.getTitle())) {
            viewHolder.issueTitleView.setText(issueModel.getTitle());
        }
        if (!Utils.isEmpty(issueModel.getBody())) {
            viewHolder.issueBodyView.setText(issueModel.getBody());
        }

        return convertView;
    }

    static class ViewHolder {
        private TextView issueTitleView;
        private TextView issueBodyView;
    }


}
