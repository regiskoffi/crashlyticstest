package com.android.crashlyticstest.model;

import java.util.Date;

public class Comment {
    private String url;
    private String htmlUrl;
    private String issueUrl;
    private Integer id;
    private User user;
    private Date createdAt;
    private Date updatedAt;
    private String body;
    String author;

    public Comment(String author, String body) {
        this.author = author;
        this.body = body;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
