package com.android.crashlyticstest.request;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ConnectionUtils {

    public static final String TAG = "HttpRequest";

    private static final String GET_METHOD = "GET";

    public static String get(String urlStr){
        InputStream is = null;
        try {
            URL url = new URL(urlStr);
            Log.d(TAG, "url = "+url);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod(GET_METHOD);
            urlConnection.setDoInput(true);
            // Starts the query
            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();
            String response;
            // Handle HTTP Call errors
            Log.d(TAG, "The response is: " + responseCode);
            if(responseCode>=200 && responseCode<300) {
                is = urlConnection.getInputStream();
            }else{
                is = urlConnection.getErrorStream();
            }
            response = inputStreamToString(is);
            return response;
        }catch (Exception ex){
            Log.e(TAG,""+ex.getMessage());
        }
        return null;
    }


    public  static String inputStreamToString(InputStream stream) throws IOException{
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
