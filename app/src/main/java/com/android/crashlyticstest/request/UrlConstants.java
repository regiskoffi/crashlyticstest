package com.android.crashlyticstest.request;

public class UrlConstants {

    public static final String BASE_URL = "https://api.github.com/repos";
    public static final String ISSUES_LIST_URL = "https://api.github.com/repos/rails/rails/issues?sort=updated";
}
